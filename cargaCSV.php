<?php
	initFunction();
	function createCSV($arra)
	{
		mb_internal_encoding("UTF-8");
		$directorio=opendir(".");//ruta actual
		while($archivo=readdir($directorio))
		{//obtenemos un archivo y luego otro sucesivamente	
			if($archivo=='resumen.csv')
			{
				$fp = fopen('resumen.csv', 'w');
				//$txd = mb_convert_encoding($arra, 'UTF-16LE', 'UTF-8');
				//fputs($fp, $bom =( chr(0xEF) . chr(0xBB)  . chr(0xBF) ));
				foreach ($arra as $fields) 
				{
				    fputcsv($fp, $fields);
				}
				fclose($fp);
				return;
			}
		}

		touch('resumen.csv');
		$fp = fopen('resumen.csv', 'w');
		//fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
				foreach ($arra as $fields) 
				{
				    fputcsv($fp, $fields);
				}
				fclose($fp);
				
			return;

	}

	function returnFechaShort($dt)
	{

		$dml_fecha = explode("-",$dt);

		$mesesShort = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
		
		$ret_fech = "";

		for ($i=0; $i < count($mesesShort); $i++) 
		{ 
			if($dml_fecha[0] == ($i+1))
			{
				$ret_fech = $mesesShort[$i].'-'.$dml_fecha[1];
				break;
			}
		}

		return $ret_fech;
	}

	function tabla1($arr)
	{
		$arr_cop = $arr;
		$mes = "";
		$t = false;

		$vcd 	= 0; 	//Vida Con Devolución x
		$pit 	= 0;	//Protección Integral Tarjeta //
		$pt 	= 0; 	//Protección Tradicional //
		$ptfi 	= 0;	//Protección Tradicional Internet Full x
		$ppfi 	= 0;	//Protección Preferente Full Internet x
		$hc		= 0;	//Hogar Contenido x 
		$rh 	= 0;	//Renta Hospitalaria //
		$bk 	= 0;	//Bike //
		$vd 	= 0;	//Vida Digital //
		$ea 	= 0;	//Emergencia Accidental //
		$mst 	= 0;	//Mascotas //
		$crd 	= 0;	//Cardiológico x
		$vp 	= 0;	//Viaje Protegido x
		$ei 	= 0;	//Egra Internet x
		$lp 	= 0;	//Linea Protegida //
		$ap 	= 0;	//Asistencia Premiun //
		$ve 	= 0;	//Vida Educacion //
		$onc 	= 0;	//Oncologico //
		$atm 	= 0;	//Automotriz x

		$tabla = "		<table>
							<thead>
								<tr>
									<th></th>
									<th>Vida Con Devolución</th>
									<th>Protección Tradicional Internet Full</th>
									<th>Protección Preferente Full Internet</th>
									<th>Hogar Contenido</th>
									<th>Cardiológico</th>
									<th>Viaje Protegido</th>
									<th>Egra Internet</th>
									<th>Automotriz</th>
								</tr>
							</thead>
							<tbody>";

		for ($i=1; $i < count($arr_cop) ; $i++)
		{ 
			
			$mes = $arr_cop[$i][4];
			$dxt = date_create($arr_cop[$i][4]);
			
			
			if($arr_cop[$i][4]==$mes)
			{
				//echo "aqui".$i;
				switch($arr_cop[$i][1])
				{
					case strpos($arr_cop[$i][1],"VIDA CON DEVOLUCIÓN CLÁSICO INTERNET"):
						$vcd++;
					break;

					case strpos($arr_cop[$i][1],"PROTECCION TRADICIONAL FULL INTERNET"):
						$ptfi++;
					break;

					case strpos($arr_cop[$i][1],"PROTECCION PREFERENTE FULL INTERNET"):
						$ppfi++;
					break;

					case strpos($arr_cop[$i][1],"INCENDIO Y ROBO INTERNET"):
						$hc++;
					break;

					case strpos($arr_cop[$i][1],"SEGURO CARDIOLOGICO INTERNET"):
						$crd++;
					break;

					case strpos($arr_cop[$i][1],"VIAJE PROTEGIDO PLUS"):
						$vp++;
					break;

					case strpos($arr_cop[$i][1],"EGRA"):
						$ei++;
					break;

					case strpos($arr_cop[$i][1],"AUTOMOTRIZ"):
						$atm++;
					break;
				}


				if($i == (count($arr_cop)-1))
				{
					//echo 'hola';
					$tabla.="<tr>";
					$tabla.="<td>". returnFechaShort(date_format($dxt,"m-y")) . "</td>";
					$tabla.="<td>". $ptfi	. "</td>";
					$tabla.="<td>".	$ppfi	."</td>";
					$tabla.="<td>". $hc	. "</td>";
					$tabla.="<td>". $crd	. "</td>";
					$tabla.="<td>". $vp	. "</td>";
					$tabla.="<td>". $ei	. "</td>";
					$tabla.="<td>". $atm	. "</td>";
					$tabla.="</tr>";
				}
			}
			else if($arr_cop[$i][4]!=$mes && $mes != "")
			{
				echo "no".$i;
				$tabla.="<tr>";
				$tabla.="<td>". returnFechaShort(date_format($dxt,"m-y")) . "</td>";
				$tabla.="<td>". $ptfi	. "</td>";
				$tabla.="<td>".	$ppfi	."</td>";
				$tabla.="<td>". $hc	. "</td>";
				$tabla.="<td>". $crd	. "</td>";
				$tabla.="<td>". $vp	. "</td>";
				$tabla.="<td>". $ei	. "</td>";
				$tabla.="<td>". $atm	. "</td>";
				$tabla.="</tr>";

				$mes = $arr_cop[$i][4];

				$vcd 	= 0;
				$ptfi 	= 0;
				$ppfi	= 0;
				$hc		= 0;
				$crd	= 0;
				$vp	 	= 0;
				$ei		= 0;
				$atm	= 0;
			}
		}

		
		$tabla .= "			</tbody>
						</table>";
		echo $tabla;
		
		//echo returnFechaShort(date("m-y"));

		//echo var_dump(explode("-",));
		//echo 12==date("m");
	}

	function conexion()
	{
		$servername = "localhost";
		$database = "libros";
		$username = "root";
		$password = "";
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $database);
		// Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}
		//echo "\n Connected successfully";

		return $conn;
	}


	function initFunction()
	{
		/*
			$archivo1; -> variable que guarda el nombre del archivo 1.csv(ex vdefin.csv)
			$arc1 -> variable de tipo array donde se guardaran los datos extraidos desde el archivo csv
			
			**posteriormente hacemos push de las cabeceras que llevara el array
		 */
		try{
		$archivo1;
		$arc1 = array();
		array_push($arc1, 
			array(	 "descTipoProducto"
					,"descSubProducto"
					,"nombre"
					,"prima"
					,"fecVigenciaInicialOperacion"
					,"rut"
					,"tipoAsegurado"
					,"plan"
				));
		
		/*
			$archivo2; -> variable que guarda el nombre del archivo 2.csv(ex vdefinnew.csv)
			$arc2 -> variable de tipo array donde se guardaran los datos extraidos desde el archivo csv
			
			**posteriormente hacemos push de las cabeceras que llevara el array
		*/
		$archivo2;
		$arc2 = array();
		array_push($arc2, array("CanaldeInicio","CanaldeTermino","RUTEJECUTIVO"));


		/*
			evaluaremos si opendir puede abrir la direccion base del directorio 
			si es asi, esta se asignara en la variable $gestor
	
			posteriormente iteraremos
			y dentro de cada iteracion de de readdir sobre el gestor de archivos
			evaluaremos si este no es falso
			en caso de serlo indicara que no hay mas archivos y se cerrara el ciclo

			en cada iteracion evaluaremos si el archivo cumple con no ser la raiz
			ni raiz de la raiz del directorio
			pero si que sea un archivo de nombre 1 o 2 csv

			despues cerraremos el directorio
			
		 */
		if ($gestor = opendir('.'))
		{
		    while (false !== ($entrada = readdir($gestor)))
		 	{
		        if ($entrada != "." && $entrada != ".." && $entrada == "vdefin.csv"  ) 
		        {
		            $archivo1 = $entrada.'';
		        }

	         	if ($entrada != "." && $entrada != ".." && $entrada == "vdefinnew.csv" )
	         	{
	         		$archivo2 = $entrada.'';
	         	}
			}
	    	closedir($gestor);
		}


		/*
			declararemos dos variables las cuales serviran para almacenar las cabeceras ubicadas
			en la posicion 0 del archivo csv 1 y 2 respectivamente
		*/
		$header1 = NULL;
		$header2 = NULL;

		/*
			declararemos variables
			las cuales tendran valor 0 de inicializacion
			estos serviran como punteros para conocer las posiciones en que se ubican dentro del csv 
			independiente que cambie el orden de las cabeceras dentro del mismo
			(solo aplica si tanto la cabecera como los datos cambian de lugar)
		 */
		$nombres = 0;
		$apellidos = 0;
		$primaTotal = 0;
		$fecVigenciaInicialOperacion = 0;
		$rut = 0;
		$rutT = 0;
		$tipoAsegurado = 0;
		$descSubProducto = 0;
		$plan = 0;
		$canalDeInicio = 0;
		$canalDeTermino = 0;
		$rutEjecutivo = 0;
		/*
			declararemos dos variables 
			csv1 y csv2 
			las cuales mapearan el contenido obtenido desde 
			la llamada callback al metodo str_getcsv que obtiene
			cada una de las filas del csv dado
			de este modo quedara un array para cada 
			fila del archivo dentro de otro array mas grande
		*/
		$csv1 = array_map('str_getcsv', file($archivo1));
		$csv2 = array_map('str_getcsv', file($archivo2));

		/*
			iteraremos sobre csv1
	 	*/
	 	$arregla_plan = 0;
		for( $ctemp = 0 ; $ctemp < count($csv1) ; $ctemp++)
		{
			/*
				y comprobaremos si el header ha sido seteado
				en caso de que no lo haya sido
				indicaremos que header contendra todas las cabeceras
				del csv1.
				esto dado que los csv vienen con cabecera en la posicion 0
		 	*/
			if(!$header1)
			{

				/*
					declararemos 2 variables adicionales que nos serviran para revisar los rut en caso de ser
					titular o no
					$rpos para un rut normal
					y $rpost para un rut titular
					esto dado que el archivo csv1 contiene varios campos con rut
		 		*/
				$header1 = $csv1[0];
				$rpos = 0; //rut position| marcador en caso de mas cabeceras de igual nombre
				$rpost = 0; //rut position titular| marcador en caso de mas cabeceras de igual nombre


				/*
					iteraremos nuevamente 
					en esta iteracion
					asignaremos la posicion de la cabecera dentro del array para ingresar directamente a esa posicion sin futuras iteraciones
					en caso de que el rut ya haya sido seteado se guardara esa posicion y ninguna otra
			 	*/
				for($sh = 0; $sh < count($header1); $sh++ )
				{
					switch ($header1[$sh]) 
					{
						case $header1[$sh]==="NOMBRES":
							$nombres = $sh;
						break;					
						case $header1[$sh]===" APELLIDOS":
							$apellidos = $sh;
						break;
						case $header1[$sh]==="PRIMA":
							$primaTotal = $sh;
						break;
						case $header1[$sh]==="FECHA INI":
							$fecVigenciaInicialOperacion = $sh;
						break;
						case $header1[$sh]==="RUT" && $rpos === 0 :
							$rpos = 1;
							$rut = $sh;
						break;
						case $header1[$sh]==="RUT TITULAR" && $rpost === 0 :
							$rpost = 1;
							$rutT = $sh;
						break;
						case $header1[$sh]==="ADICIONAL":
							$tipoAsegurado = $sh;
						break;
						case $header1[$sh]==="NOMBRE DEL PRODUCTO":
							$descSubProducto = $sh;
						break;
						case $header1[$sh]==="PLAN VIAJE":
							$plan = $sh;
						break;
					}
				}
			}
			else/*en caso de que ya fuera seteada la variable header
			procederemos a hacer un push(ingresar) de datos en el array $arc1 (archivo1)
			ingresando directamente por cada una de las posiciones antes guardadas en la sub iteracion*/
			{
				// //echo var_dump($csv1);
				// //
				// $arrcop = $csv1;
				// $arrayk = $csv1[0];
				// $arrayxx = array_fill_keys($arrayk, null);

				// echo "<pre>";
				// echo var_dump($arrayxx);
				// echo "</pre>";
				// // for($kj= 1 ; $kj < count($arrcop);$kj++)
				// // {
				// // 	// echo "<pre>";
				// // 	// echo count($arrayk[$]);
				// // 	// echo "</pre>";
				// // 	$arrayxx[$arr];
				// // }


				// $plan_titular = "";
				// $fecha_titular = "";
				// if($arregla_plan==0)
				// {
				// 	for ($hh = (count($csv1)-2) ; $hh > -1 ; $hh-- ) 
				// 	{
				// 		//echo $hh."<br>";
				// 		//echo $csv1[$hh][$tipoAsegurado]."<br>";
				// 		if($csv1[$hh][$descSubProducto]=="VIAJE PROTEGIDO PLUS" && $csv1[$hh][$tipoAsegurado] == "N" )
				// 		{
				// 			//echo "entro ".$csv1[$hh][$plan]."<br>";
				// 			$plan_titular = $csv1[$hh][$plan];
				// 			$fecha_titular = $csv1[$hh][$fecVigenciaInicialOperacion];
				// 		}
				// 		else if( $csv1[$hh][$fecVigenciaInicialOperacion]==$fecha_titular && $csv1[$hh][$tipoAsegurado] != "N")
				// 		{
				// 			// echo "xxxxx";
				// 			// echo "ctm ".$plan_titular." <br>";

				// 			$csv1[$hh][$plan] = $plan_titular;
				// 		}
				// 	}
				// 	$arregla_plan++;
				// }
				// echo "<pre>";
				// echo var_dump($csv1);
				// echo "</pre>";
				array_push($arc1,
							array
							(
								modDescTipoProd($csv1[$ctemp][$descSubProducto]),
								modDescProd($csv1[$ctemp][$descSubProducto],$rut,$csv1[$ctemp][$rutT],$csv1,$descSubProducto,$csv1[$ctemp][$fecVigenciaInicialOperacion]
										,$fecVigenciaInicialOperacion),
								modNombre($csv1[$ctemp][$nombres],$csv1[$ctemp][$apellidos]),
								$csv1[$ctemp][$primaTotal],//str_replace(".", ",", $csv1[$ctemp][$primaTotal]),
								str_replace('/', '-', $csv1[$ctemp][$fecVigenciaInicialOperacion]),
								modRut($csv1[$ctemp][$rut]),
								($csv1[$ctemp][$tipoAsegurado]=="N"?"TITULAR":"ADICIONAL"),
								($csv1[$ctemp][$tipoAsegurado]!="N"?(modPlan(
										$csv1[$ctemp][$descSubProducto]
										,$rut
										,$csv1[$ctemp][$rutT]
										,$csv1
										,$descSubProducto
										// ,$csv1[$ctemp][$plan]
										,$plan						
										,$csv1[$ctemp][$fecVigenciaInicialOperacion]
										,$fecVigenciaInicialOperacion
									   )):$csv1[$ctemp][$plan])

								
								
								
							));
			}
		}

		//echo count($arc1);

		/*aplicaremos el mismo metodo pero para con el archivo o array de csv2*/
		for( $ctemp2 = 0 ; $ctemp2 < count($csv2) ; $ctemp2++)
		{
			if(!$header2)
			{

				$header2 = $csv2[0];
				$rpos = 0;
				$rpost = 0;

				for($sh2 = 0; $sh2 < count($header2); $sh2++ )
				{
					switch ($header2[$sh2]) 
					{
						case $header2[$sh2]==="INICIO VENTA":
							$canalDeInicio = $sh2;
						break;					
						case $header2[$sh2]==="FIN VENTA":
							$canalDeTermino = $sh2;
						break;
						case $header2[$sh2]==="RUT EJECUTIVO":
							//echo $sh2;
							$rutEjecutivo = $sh2;
						break;
					}
				}
			}
			else
			{
				//echo $rutEjecutivo;
				//echo $csv2[$ctemp2];
				//echo $csv2[$ctemp2][$rutEjecutivo];
				array_push($arc2, 
						array(
								trim($csv2[$ctemp2][$canalDeInicio]),
							  	trim($csv2[$ctemp2][$canalDeTermino]),
							 	modRut2($csv2[$ctemp2][$rutEjecutivo])
					 		)
		  				);
			}
		}


		/*
			declararemos una variable $i para iterar luego
			declararemos una variable $arrayfinal donde se
			uniran los 2 arrays anteriores

			iteraremos por cada uno de los elementos del array $arc1
			y agregaremos al array final la union del sub array de $arc1
			y el sub array de $arc2 en la posicion del puntero externo
		*/
		$i = 0;
		$arrayfinal = array();
		foreach ($arc1 as $ll) 
		{
			$arrayfinal[] = array_merge($ll,$arc2[$i]);
			$i++;
		}

		// foreach ($arrayfinal as $key => $value)
		// {
		// 	echo "<pre>";
		// 	echo var_dump($value);
		// 	echo "</pre>";
		// }

		// echo "<pre>";
		// echo var_dump($arrayfinal);
		// echo "</pre>";

	 	/*
	 		como ultimo punto llamaremos a la funcion nonquery 
	 		para completar el proceso de ingresar los datos del csv a la bd
	 	 */
		nonquery($arrayfinal);

		// createCSV($arrayctm);
		//showTable($arrayctm);
		//tabla1($arrayctm2);
		}
		catch(Exception $e)
		{
			echo $e->message();
		}
	
	}
	/**
	 * [nonquery este metodo tiene unicamente por fin ingresar los datos nuevos de los csv a la bd]
	 * @param  [array] $arr [este array es la combinacion de los datos del csv1 y 2]
	 * @return [void]      [solo entrega un mensaje en caso de que se hayan ingresado los datos de forma correcta o un mensaje de error por cada dato]
	*/
	function nonquery($arr)
	{

		$sql = ""; // declaramos la variable $sql donde ira la consulta en duro
		$cc = conexion(); //declaramos $cc donde guardaremos la conexion desde el metodo initial

		$sql .= "TRUNCATE TABLE ResumenVenta;";
		for ($i=1; $i < count($arr) ; $i++) 
		{ 

			$sql .= "INSERT INTO 
			ResumenVenta 
			(
				ResumenVentaId,
				ResumenVentaDescTipoProducto,
				ResumenVentaDescSubProducto,
				ResumenVentaNombre,
				ResumenVentaPrima,
				ResumenVentaFecVigenciaInicialOperacion,
				ResumenVentaRut,
				ResumenVentaTipoAsegurado,
				ResumenVentaCanalDeInicio,
				ResumenVentaCanalDeTermino,
				ResumenVentaRutEjecutivo,
				ResumenVentaPlan
			)
			VALUES(null,
			'".$arr[$i][0]."','".
			$arr[$i][1]."','".
			$arr[$i][2]."',".
			$arr[$i][3].",'".
			date("Y-m-d", strtotime($arr[$i][4]))."','".
			$arr[$i][5]."','".
			$arr[$i][6]."','".
			//$arr[$i][7]."','".
			$arr[$i][8]."','".
			$arr[$i][9]."','".
			$arr[$i][10]."','".
			$arr[$i][7]."');";
		}

		//echo $sql;


		if(mysqli_multi_query($cc,$sql))
		{
			echo "\n Guardado Correctamente";
			echo mysqli_close($cc)? "\n conexion cerrada correctamente":"no se ha podido cerrar la conexion";
		}
		else
		{
			echo "\n Error: " . $sql . "<br>" . mysqli_error($cc);
			echo mysqli_close($cc)? " hubo un error y se ha cerrado la conexion correctamente":" hubo un error y no se ha podido cerrar la conexion";
		}
	}

	// function nonquery2($arr)
	// {
	// 	$stmt = $conn->prepare("INSERT INTO ResumenVenta
	// 							(
	// 								ResumenVentaDescTipoProducto,
	// 								ResumenVentaDescSubProducto,
	// 								ResumenVentaNombre,
	// 								ResumenVentaPrima,
	// 								ResumenVentaFecVigenciaInicialOperacion,
	// 								ResumenVentaRut,
	// 								ResumenVentaTipoAsegurado,
	// 								ResumenVentaCanalDeInicio,
	// 								ResumenVentaCanalDeTermino,
	// 								ResumenVentaRutEjecutivo
	// 							)	
	// 							VALUES
	// 							(
	// 								?,?,?,?,?,?,?,?,?,?
	// 							)");
	// 	$stmt->bind_param("sssdssssss"
	// 	);
	// }

	/**
	 * [showTable Funcion para simular la tabla resumen de ventas del consolidado de manera programatica]
	 * @param  [array] $arr [array de datos desde el csv final]
	 * @return [void]      [no retorna, si no que muestra por pantalla los datos de la tabla conformada]
	 */
	function showTable($arr)
	{	
		$tabla = "		<table>
							<thead>
								<tr>
									<th>descTipoProducto</th>
									<th>descSubProducto</th>
									<th>Nombre</th>
									<th>PrimaTotal</th>
									<th>Fecvigenciainicialoperacion</th>
									<th>RUT</th>
									<th>TipoAsegurado</th>
									<th>CanaldeInicio</th>
									<th>CanaldeTermino</th>
									<th>RUTEJECUTIVO</th>
								</tr>
							</thead>
							<tbody>";

		for($al = 1; $al < count ($arr); $al++ ) 
		{
			$tabla.="<tr>";
			$tabla.="<td>". $arr[$al][0]	. "</td>";
			$tabla.="<td>". $arr[$al][1]	. "</td>";
			$tabla.="<td>".	$arr[$al][2]	. "</td>";
			$tabla.="<td>". $arr[$al][3]	. "</td>";
			$tabla.="<td>". $arr[$al][4]	. "</td>";
			$tabla.="<td>". $arr[$al][5]	. "</td>";
			$tabla.="<td>". $arr[$al][6]	. "</td>";
			$tabla.="<td>". $arr[$al][7]	. "</td>";
			$tabla.="<td>". $arr[$al][8]	. "</td>";
			$tabla.="<td>". $arr[$al][9]	. "</td>";
			$tabla.="</tr>";
		}

		$tabla .= "			</tbody>
						</table>";

		echo $tabla;
		
	}

	/**
	 * [modRut Modifica el rut para ingresarlo en la base de datos en el formato deseado]
	 * @param  [string] $rut [rut si formatear desde el csv]
	 * @return [string]      [rut formateado]
	 */
	function modRut($rut)
	{
		$rc = "";
		$rc = $rut;
		trim($rc);
		$rc = str_replace("-","",$rc);

		if($rc[0]==0 || $rc[0]=="0")
		{
			$rc = substr($rc,1);
		}

		// if($rc[strlen($rc)-1]==="0" && $rc[strlen($rc)-2]==="0")
		// {
		// 	// $ffff = array();
		// 	// $mult = 2;

		// 	// for ($i=count($rc); $i > 0 ; $i--) 
		// 	// { 

		// 	// 	if($mult<=7)
		// 	// 	{
		// 	// 		$array_push($ffff,((int)$rc[i]*$mult));
		// 	// 		$mult++;
		// 	// 	}
		// 	// 	else
		// 	// 	{
		// 	// 		$mult = 2;
		// 	// 		$array_push($ffff,((int)$rc[i]*$mult));
		// 	// 	}
		// 	// }

		// 	$rc = substr($rc, 0,$rc[strlen($rc)-2]);
		// }
		// else
		// {
			
		// }

		$tRC = str_split($rc);

			for ($i=0; $i <count($tRC) ; $i++)
			{ 
				if($tRC[$i]!="0")
				{
					$rc=substr($rc, $i);
					break;
				}
			}

		if(strlen($rc)<9)
		{
			//echo $rc."   ".substr($rc, -1)."<br>";
			$dv = substr($rc, -1);
			$r = substr($rc, 0,-1);

			$rc = "0".$r."-".$dv;
		}
		else
		{
			$dv = $rc[-1];
			$r = substr($rc, 0,-1);

			$rc = $r."-".$dv;
		}
		// $rc = (($rc[0]==="0"  && strlen($rc)>8) ? substr($rc, 1): $rc);
		// $rc = strlen($rc)<9 ? "0".substr($rc, 0,(strlen($rc)-1))."-".$rc[-1] : substr($rc, 0,(strlen($rc)-1))."-".$rc[-1];
		// $rc = str_replace("-", "", $rc);

		// $dv = $rc[-1];
		// $rc = substr($rc, 0 , strlen($rc)-1)."-".$dv;
		return $rc;
	}

	/**
	 * [modRut2 Modifica el rut para ingresarlo en la base de datos en el formato deseado version 2]
	 * @param  [string] $rut [rut si formatear desde el csv]
	 * @return [string]      [rut formateado]
	 */
	function modRut2($rut): String
	{
		//intentamos
		try
		{
			//declaramos una variable string vacio
			//declaramos una variable que almacenara el array resultante de la division en caracteres del rut entrante
			$rc = "";
			$k = str_split($rut);

			/*guardara la posicion del primer caracter distinto a 0*/
			$kpos = 0;
			
			//en caso de no haber otros numeros que no sean 0 servira para decidir que hacer con el espacio de 0's
			$contadorZ = 0;


			//iteramos sobre los caracteres del rut
			for ($i=0; $i <count($k) ; $i++)
		 	{ 
		 		//si encontramos un caracter distinto a 0
		 		//guardaremos la posicion de este y pararemos la iteracion
		 		//en caso contrario acumularemos en contadorZ(haciendo alucion a los 0's del rut)
				if($k[$i]!="0")
				{
					$kpos = $i;
					break;
				}
				else
				{
					$contadorZ++;
				}
			}

			//verificamos la cantidad de caracteres es igual a la cantidad del contador
			//si es asi diseccionamos el rut y lo formateamos a lo deseado
			//en caso contrario revolvemos string vacia
			if(count($k)!= $contadorZ)
			{
				$rc = substr($rut, $kpos);
				$rc = str_replace("-","",$rc);
				$rc = str_replace(".","",$rc);
				$rc = str_replace(",","",$rc);

				if(strlen($rc)<9)
				{
					$dv = $rc[-1];
					$r = substr($rc, 0,-1);
					$rc = "0".$r."-".$dv;
				}
				else
				{
					$dv = $rc[-1];
					$r = substr($rc, 0,-1);

					$rc = $r."-".$dv;
				}
			}
			else
			{
				$rc = "";
			}
			return $rc;
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	
	/**
	 * [modDescTipoProd este metodo deja los tipos de seguro en el formato deseado]
	 * @param  [string] $descProducto [string a formatear]
	 * @return [string]               [string formateada]
	 */
	

	function veriRut($rut)
	{
		$ffff = array();
		$mult = 2;
		$suma = 0;

		for ($i=count($rc); $i > 0 ; $i--) 
		{ 
			if($mult<7)
			{
				$suma+= ((int)$rc[i]*$mult);
				$mult++;
			}
			else
			{
				$mult = 2;
				$suma+= ((int)$rc[i]*$mult);
				//$array_push($ffff,((int)$rc[i]*$mult));
			}
		}

		$cal = 11-($suma % 11);


		if($rut[-1]===$cal)
		{
			return true;
		}
		else
		{
			return false;
		}


	}

	function modDescTipoProd($descProducto)
	{
		//aseguramos la variable como local de la funcion
		//
		//evaluamos si dentro de el string se encuentra la palabra proteccion
		//en caso de encontrarla
		//asignamos la descripcion a devolver como individual small
		//en caso contrario devolveremos la string como individual
		$desc = $descProducto;
		if(stripos($desc, "PROTECCION")!==false)
		{	
			$desc = "INDIVIDUAL SMALL";
		}
		else
		{
			$desc = "INDIVIDUAL";
		}
		return $desc;
	}

	/**
	 * [modDescTipoProd metodo para dejar seteado el plan de viaje al adicional segun su titular]
	 * @param  [string] $descProducto 	[descripcion producto (nombre seguro)]
	 * @param  [int] $rutp 				[rut titular pointer(puntero posicion item rut titular)]
	 * @param  [string] $rut 			[rut del titular]
	 * @param  [array] $arr 			[arreglo de ]
	 * @param  [string] $descp 			[descripcion pointer(puntero posicion item descripcion)]
	 * @param  [string] $planp 			[plan pointer(puntero posicion item plan)]
	 * @param  [string] $fec 			[fecha del inicio vigencia de seguro]
	 * @param  [string] $fecp 			[fecha pointer(puntero posicion item fecha)]
	 * @return [string]               	[plan que el adicional debiese tener , string formateada]
	*/
	function modPlan($descProducto,$rutp,$rut,$arr,$descp,$planp,$fec,$fecp)
	{
		//seteamos la variable local $desc en vacio
		//asignamos el valor del parametro de descripcion a la variable local
		$desc = "";
		$desc = $descProducto;


		//seteamos el plan de viaje en vacio
		//asignamos el plan correspondiente 
		$plan = "";
		$plan = $plan;

		//$rr = $rutT;
		//seteamos la variable local $arcs con el array externo recibido
		$arcs = $arr;
		
		//evaluamos si descripcion es distinto de false
		//seteamos $variable local como VIAJE PROTEGIDO PLUS
		if(stripos($desc, "VIAJE PROTEGIDO PLUS")!==false)
		{	
			$desc = "VIAJE PROTEGIDO PLUS";
		}

		//iteramos sobre los elementos del array
		//evaluamos si el rut que a iterado corresponde  con el rut pasado como parametro
		//evaluamos si la descripcion es vacia
		//evaluamos si la fecha corresponde con la fecha pasada
		//y finalmente evaluamos si el seguro es de tipo VIAJE PROTEGIDO PLUS
		//en caso de serlo recuperamos en la variable local el plan a retornar
		for ($ox=1; $ox < count($arcs) ; $ox++) 
		{ 
			if($arcs[$ox][$rutp]===$rut 
				&& $desc === "" 
				&& $arcs[$ox][$fecp] ===$fec  
				&& $arcs[$ox][$descp] == "VIAJE PROTEGIDO PLUS"
			)
			{
				echo count($arcs[$ox]);
				$plan = $arcs[$ox][$planp];
			}
		}

		

		//retornamos el plan formateandolo a sin guion bajo pero con espacio
		return str_replace("_"," ",$plan);
	}

	/**
	 * [modDescProd modifica la descripcion del producto segun lo encontrado]
	 * @param  [string] $descProducto [descripcion del producto]
	 * @param  [string] $rutT         [posicion cabecera rut titular]
	 * @param  [string] $rut          [rut del aludido]
	 * @param  [string] $arr          [array del csv]
	 * @param  [string] $descp        [cabecera de posicion del item dentro del array correspondiente a la descripcion del producto]
	 * @return [string]               [devuelve la descripcion el producto formateada]
	 */
	function modDescProd($descProducto,$rutT,$rut,$arr,$descp,$fec,$fecp)
	{
		
		/*
		guardamos descripcion, rut del titular y array entrantes en variables locales
		 */
		$desc = "";
		$desc = $descProducto;
		$rr = $rutT;
		$arcs = $arr;
		
		/*
		evaluamos si
		se encuentra la substring vida con bonificacion 
		modificamos la string saliente

		igualmente si encontramos la substring egra
		 */
		if(strpos($desc, "VIDA CON BONIFICACION")!==false)
		{	
			$desc = "VIDA CON DEVOLUCIÓN CLÁSICO INTERNET";
		}

		if(strpos($desc, "EGRA")!==false)
		{	
			$desc = "EGRA";
		}

		/*
		posteriormente iteramos el arreglo solicitado
		y evaluamos si
		el arreglo en la posicion del rut titular 
		si el rut entregado es igual al rut encontrado en la posicion del titular
		y ademas esta vacia la descripcion
		ademas la fecha es igual a la del titular
		se igualara el tipo de seguro al de su titular

		 */
		

		for ($ox=1; $ox < count($arcs) ; $ox++) 
		{ 
			if($arcs[$ox][$rutT]===$rut 
				&& $desc === "" 
				&& $arcs[$ox][$fecp] ===$fec
				&& $arcs[$ox][231] !=null
			)
			{	
				echo $arcs[$ox][$rutT]."\n";
				echo $rut."\n";
				echo $arcs[$ox][$descp]."\n";
				echo "<br>";
				$desc = $arcs[$ox][$descp];
			}
		}

		//retornamos la descripcion decodificando en caso de ser utf8 el conjunto de caracteres
		return utf8_decode($desc);
	}

	/*
		igual caso que la funcion anterior
		se exceptua el uso de decodificacion del string y se entrega tal cual era
		con su conjunto de caracteres
	 */
	function modDescProdNonISO($descProducto,$rutT,$rut,$arr,$descp)
	{
		$desc = "";
		$desc = $descProducto;
		$rr = $rutT;
		$arcs = $arr;
		
		if(stripos($desc, "VIDA CON BONIFICACION")!==false)
		{	
			$desc = "VIDA CON DEVOLUCIÓN CLÁSICO INTERNET";
		}

		if(stripos($desc, "EGRA")!==false)
		{	
			$desc = "EGRA";
		}

		for ($ox=1; $ox < count($arcs) ; $ox++) 
		{ 
			if($arcs[$ox][$rutT]===$rut && $desc == "")
			{
				$desc = $arcs[$ox][$descp];
			}
		}

		return $desc;
	}


	/**
	 * [modNombre funcion que tiene unicamente por fin concatenar los nombres y apellidos en uno]
	 * @param  [string] $nombres   [nombre del cliente]
	 * @param  [string] $apellidos [apellido cliente]
	 * @return [string]            [retornamos la concatenacion de string y aplicamos igualmente formateo del string para tener la capitalizacion de las letras iniciales de cada conjunto de string no vacio]
	 */
	function modNombre($nombres,$apellidos)
	{
		// $nombres = strtolower($nombres);
		// $apellidos = strtolower($apellidos);

		// $nombres = ucwords($nombres);
		// $apellidos =  ucwords($apellidos);

		$nombre = $nombres." ".$apellidos;

		
		$string = mb_strtolower($nombre, 'UTF-8');
		$string = ucwords($string);
		// $string = mb_convert_encoding($nombre,'ISO-8859-1', 'CP1252');

		// $string = iconv('CP1252', 'UTF-8', $string);
		// $string = mb_convert_encoding($string, 'CP1252','ISO-8859-1');
		// $string = mb_convert_encoding($string, 'ISO-8859-1','UTF-8');
	   	// $string = $nombre;
	   	
		// echo "<pre>";
		// echo $string;
		// echo "</pre>";
		//$string =  ucwords($string);
		return utf8_decode($string);
	}
?>